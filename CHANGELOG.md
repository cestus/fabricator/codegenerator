
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.0.5"></a>
## [v0.0.5](https://gitlab.com/cestus/fabricator/codegenerator/compare/v0.0.4...v0.0.5)

### Fix

* !!ifor created a crash because of a invalid condition


<a name="v0.0.4"></a>
## [v0.0.4](https://gitlab.com/cestus/fabricator/codegenerator/compare/v0.0.3...v0.0.4)

### Feat

* Allow better if conditions in files


<a name="v0.0.3"></a>
## [v0.0.3](https://gitlab.com/cestus/fabricator/codegenerator/compare/v0.0.2...v0.0.3)

### Chore

* add fabricator definition and regen

### Feat

* Add ToUpper and ToLower for templates


<a name="v0.0.2"></a>
## [v0.0.2](https://gitlab.com/cestus/fabricator/codegenerator/compare/v0.0.1...v0.0.2)


<a name="v0.0.1"></a>
## v0.0.1

### Feat

* add ci
* remove dependency on swag
* codegenerator

